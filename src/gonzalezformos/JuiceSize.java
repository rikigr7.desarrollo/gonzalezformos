/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package gonzalezformos;

/**
 * 100 300 500
 *
 * @author Game
 */
public enum JuiceSize {
    small(100),
    medium(300),
    large(500);

    public final int size;

    private JuiceSize(int size) {
        this.size = size;
    }
}
