/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gonzalezformos;

import com.inventory.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Game
 */
public class Menu {

    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    Strawberry strawberry = new Strawberry();
    Banana banana = new Banana();
    Mango mango = new Mango();
    Ice ice = new Ice();
    Milk milk = new Milk();
    Sugar sugar = new Sugar();

    public void StarMenu() {
        String opcion;
        System.out.println("Ricardo González' juices");
        EditInventoryFull();
        System.out.println("");

        do {
            opcion = ShowMenu();

            switch (opcion) {
                case "1" -> {
                    EditInventory(ShowOpcionInventary());
                }
                case "2" -> {
                    ShowInventory();
                }
                case "3" -> {
                    PrepareJiuce(ShowOpcionJuices());
                }
                case "0" -> {

                }
                default ->{
                    System.out.println("Wrong choice");
                    System.out.println("");
                }
            }
        } while (!opcion.equals("0"));
    }

    public void PrepareJiuce(String opcion) {

        switch (opcion) {
            case "1" -> {
                strawberry.PrepareJuice(JuiceSize.medium);
            }
            case "2" -> {
                banana.PrepareJuice(JuiceSize.medium);
            }
            case "3" -> {
                mango.PrepareJuice(JuiceSize.medium);
            }
            default ->
                System.out.println("Don't choose a juice");
        }
    }

    public String ShowMenu() {
        System.out.println("Ricardo González' juices");
        System.out.println("/************MENU************/");
        System.out.println("1. Edit Inventory");
        System.out.println("2. Show Inventory");
        System.out.println("3. Sell a drink ");
        System.out.println("0. Exit");
        System.out.print("Enter your option ");
        String opcion = "Emply";
        try {
            opcion = br.readLine();
            return opcion;
        } catch (IOException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
            return opcion;
        } finally {
            System.out.println("Your option is: " + opcion);

        }
    }

    public String ShowOpcionJuices() {
        System.out.println("");
        System.out.println("/************JUICES************/");
        System.out.println("1. Strawberry");
        System.out.println("2. Banana");
        System.out.println("3. Mango");
        System.out.println("0. Return");
        System.out.print("Enter your option ");
        String opcion = "Emply";
        try {
            opcion = br.readLine();
            return opcion;
        } catch (IOException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
            return opcion;
        } finally {
            System.out.println("Your option is: " + opcion);

        }
    }

    public String ShowOpcionInventary() {
        System.out.println("");
        System.out.println("/************INVENTARY************/");
        System.out.println("1. Strawberry");
        System.out.println("2. Banana");
        System.out.println("3. Mango");
        System.out.println("4. Ice");
        System.out.println("5. Milk");
        System.out.println("6. Sugar");
        System.out.print("Enter your option ");
        String opcion = "Emply";
        try {
            opcion = br.readLine();
            return opcion;
        } catch (IOException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
            return opcion;
        } finally {
            System.out.println("Your option is: " + opcion);

        }
    }

    public void ShowInventory() {

        System.out.println("");
        System.out.println("Your inventory is: ");
        System.out.println("Strawberry: " + strawberry.getAmountBDD() + " g");
        System.out.println("Banana: " + banana.getAmountBDD() + " g");
        System.out.println("Mango: " + mango.getAmountBDD() + " g");
        System.out.println("Ice: " + Ice.getAmountBDD() + " ml");
        System.out.println("Milk: " + Milk.getAmountBDD() + " ml");
        System.out.println("Sugar: " + Sugar.getAmountBDD() + " g");
        System.out.println("Press Enter key to continue...");
        Scanner teclado = new Scanner(System.in);
        teclado.nextLine();

    }

    public void EditInventoryFull() {

        try {
            String temp_amount;
            System.out.print("Please, Enter quantity of Strawberry in inventory: ");
            temp_amount = br.readLine();
            strawberry.setAmountBDD(Integer.parseInt(temp_amount));

            System.out.print("Please, Enter quantity of Banana in inventory: ");
            temp_amount = br.readLine();
            banana.setAmountBDD(Integer.parseInt(temp_amount));

            System.out.print("Please, Enter quantity of Mango in inventory: ");
            temp_amount = br.readLine();
            mango.setAmountBDD(Integer.parseInt(temp_amount));

            System.out.print("Please, Enter quantity of Ice in inventory: ");
            temp_amount = br.readLine();
            Ice.setAmountBDD(Integer.parseInt(temp_amount));

            System.out.print("Please, Enter quantity of Milk in inventory: ");
            temp_amount = br.readLine();
            Milk.setAmountBDD(Integer.parseInt(temp_amount));

            System.out.print("Please, Enter quantity of Sugar in inventory: ");
            temp_amount = br.readLine();
            Sugar.setAmountBDD(Integer.parseInt(temp_amount));

        } catch (IOException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void EditInventory(String opcion) {

        try {
            String temp_amount;
            
            switch (opcion) {
                case "1" -> {
                    System.out.print("Please, Enter quantity of Strawberry in inventory: ");
                    temp_amount = br.readLine();
                    strawberry.setAmountBDD(Integer.parseInt(temp_amount));
                }
                case "2" -> {
                    System.out.print("Please, Enter quantity of Banana in inventory: ");
                    temp_amount = br.readLine();
                    banana.setAmountBDD(Integer.parseInt(temp_amount));
                }
                case "3" -> {
                    System.out.print("Please, Enter quantity of Mango in inventory: ");
                    temp_amount = br.readLine();
                    mango.setAmountBDD(Integer.parseInt(temp_amount));
                }
                case "4" -> {
                    System.out.print("Please, Enter quantity of Ice in inventory: ");
                    temp_amount = br.readLine();
                    Ice.setAmountBDD(Integer.parseInt(temp_amount));
                }
                case "5" -> {
                    System.out.print("Please, Enter quantity of Milk in inventory: ");
            temp_amount = br.readLine();
            Milk.setAmountBDD(Integer.parseInt(temp_amount));
                }
                case "6" -> {
                    System.out.print("Please, Enter quantity of Sugar in inventory: ");
            temp_amount = br.readLine();
            Sugar.setAmountBDD(Integer.parseInt(temp_amount));
                }
                default -> System.out.print("Please, Enter quantity of Strawberry in inventory: ");
            }
        } catch (IOException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
