/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory;

import com.BDD.BDD;

/**
 *
 * @author Game
 */
public class Ice extends Fruits {

    public static int getAmountBDD() {
        return (int) BDD.Ice[0];
    }

    public static void setAmountBDD(int amount) {
        BDD.Ice[0] = amount;
    }

    public static void restAmountBDD(int amount) {
        int newValue = getAmountBDD() - amount;
        setAmountBDD(newValue);
    }
}
