/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory;

import com.BDD.BDD;
import gonzalezformos.JuiceSize;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Game
 */
public class Mango extends Fruits {

    public Mango() {
        fruitGrams = 140;//g
        fruitMilliliters = 100;//ml
    }

    public int getAmountBDD() {
        return (int) BDD.Mango[0];
    }

    public void setAmountBDD(int amount) {
        BDD.Mango[0] = amount;
    }

    public void restAmountBDD(int amount) {
        int newValue = getAmountBDD() - amount;
        setAmountBDD(newValue);
    }

    public void PrepareJuice(JuiceSize size) {

        String nameFruit = this.getClass().getSimpleName();

        //Juice relacion
        int juice = 100;
        int compare = size.size / juice;

        ValidateFruit(compare);
        List<String> validate = ValidateInventory(compare);

        if(validate.isEmpty()){
            //OK
            Prepare(nameFruit);
            System.out.println("*****************");
            System.out.println("Prepared juice !! !! !!  OK");
            System.out.println("*****************");
        }else{
            //Error
            for (String sms : validate) {
                System.out.println(sms);
            }
        }
        System.out.println("");
        System.out.println("Press Enter key to continue...");
        Scanner teclado = new Scanner(System.in);
        teclado.nextLine();
    }

    public void ValidateFruit(int compare){

        //Blended fruit
        int temp_Fruit = FruitForJuice(50, fruitGrams, fruitMilliliters) * compare;
        if (temp_Fruit > getAmountBDD()) {
            fruit = 0;
        }else{
            fruit = temp_Fruit;
        }
    }
}
