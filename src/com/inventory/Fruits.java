/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.inventory;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Game
 */
public class Fruits {

    int fruitGrams;//g
    int fruitMilliliters;//ml

    public int fruit;
    private int ice;
    private int milk;
    private int sugar;

    public int FruitForJuice(int juiceNeed, int fruitGrams, int fruitMilliliters) {
        return juiceNeed * fruitGrams / fruitMilliliters;
    }

    public List<String> ValidateInventory(int compare) {

        List<String> answer = new ArrayList<>();

        ice = 0;
        milk = 0;
        sugar = 0;

        //Blended fruit
        if (fruit >= 0) {
            answer.add("Don't have enough "+ this.getClass().getSimpleName());
        }

        //Ice
        ice = 30 * compare;
        if (ice > Ice.getAmountBDD()) {
            answer.add("Don't have enough Ice");
        }

        //Milk
        milk = 20 * compare;
        if (milk > Milk.getAmountBDD()) {
            answer.add("Don't have enough Milk");
        }

        //Sugar
        sugar = 8 * compare;
        if (sugar > Sugar.getAmountBDD()) {
            answer.add("Don't have enough Sugar");
        }
        return answer;
    }

    public void Prepare(String nameFruit) {
        switch (nameFruit) {
            case "Strawberry" ->
                new Strawberry().restAmountBDD(fruit);
            case "Banana" ->
                new Banana().restAmountBDD(fruit);
            case "Mango" ->
                new Mango().restAmountBDD(fruit);
            default -> {
            }

        }
        Milk.restAmountBDD(milk);
        Ice.restAmountBDD(ice);
        Sugar.restAmountBDD(sugar);
    }
}
